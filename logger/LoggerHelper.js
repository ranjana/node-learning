const { createLogger, format, transports } = require('winston');
const { combine, timestamp, printf,prettyPrint } = format;

const isProd = true;
const myFormat =printf(({ level, message,timestamp }) => {
    return `${timestamp} ${level}: ${message}`;
});

const    logger =createLogger({
        level: 'debug',
        defaultMeta : {app : 'test-service'},
        format:  combine(
            timestamp({format : 'YYYY-DD-MM:HH-mm-ss'}),
            myFormat,
            //it print in json
            prettyPrint(),
        ),
        transports: [
            isProd ? (new transports.File({filename: 'combined.log'})
                // ,new transports.File({filename: 'error.log',level :'error'})
            ) : new transports.Console(),
        ],
    });
//
module.exports =logger;
